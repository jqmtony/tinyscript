package org.tinygroup.tinyscript.springmvc;

import org.springframework.http.MediaType;
import org.tinygroup.tinyscript.mvc.StringConvertHandler;

public abstract class MediaTypeConvertHandler extends StringConvertHandler<MediaType>{

	private Class<?> clazz;  //接受的结果对象类型
	
	private MediaType mediaType;  //接受的转换对象协议
	
	public MediaTypeConvertHandler(Class<?> clazz,String accept){
		this(clazz,MediaType.valueOf(accept));
	}
	
    public MediaTypeConvertHandler(Class<?> clazz,MediaType mediaType){
		this.clazz = clazz;
		this.mediaType = mediaType;
	}

	public boolean isMatch(Object obj, MediaType accept) {
		if(obj==null || (obj.getClass().equals(clazz) || clazz.isInstance(obj))==false){
		   return false;
		}
		return accept.includes(mediaType);
	}

}
