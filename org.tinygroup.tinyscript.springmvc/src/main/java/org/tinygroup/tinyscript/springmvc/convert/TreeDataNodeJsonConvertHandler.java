package org.tinygroup.tinyscript.springmvc.convert;

import org.springframework.http.MediaType;
import org.tinygroup.tinyscript.springmvc.MediaTypeConvertHandler;
import org.tinygroup.tinyscript.tree.impl.DataNodeUtil;
import org.tinygroup.tinyscript.tree.impl.TreeDataNode;

/**
 * TreeDataNode转换Json
 * @author yancheng11334
 *
 */
public class TreeDataNodeJsonConvertHandler extends MediaTypeConvertHandler{

	public TreeDataNodeJsonConvertHandler() {
		super(TreeDataNode.class, MediaType.APPLICATION_JSON);
	}

	protected String convert(Object obj, MediaType accept) {
		TreeDataNode tree = (TreeDataNode) obj;
		return DataNodeUtil.toJson(tree);
	}

	protected String getContentType() {
		return MediaType.APPLICATION_JSON_VALUE;
	}

}
