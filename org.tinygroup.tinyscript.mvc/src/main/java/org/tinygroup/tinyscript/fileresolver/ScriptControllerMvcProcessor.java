package org.tinygroup.tinyscript.fileresolver;

import org.tinygroup.fileresolver.impl.AbstractFileProcessor;
import org.tinygroup.logger.LogLevel;
import org.tinygroup.tinyscript.mvc.TinyScriptMvcManager;
import org.tinygroup.tinyscript.mvc.config.ScriptControllerConfigs;
import org.tinygroup.vfs.FileObject;
import org.tinygroup.xstream.XStreamFactory;

import com.thoughtworks.xstream.XStream;

/**
 * 脚本mvc配置扫描器
 * @author yancheng11334
 *
 */
public class ScriptControllerMvcProcessor extends AbstractFileProcessor {

	private static final String XSTREAM_NAME = "scriptmvc";
	
	private TinyScriptMvcManager tinyScriptMvcManager;
	
	public TinyScriptMvcManager getTinyScriptMvcManager() {
		return tinyScriptMvcManager;
	}

	public void setTinyScriptMvcManager(TinyScriptMvcManager tinyScriptMvcManager) {
		this.tinyScriptMvcManager = tinyScriptMvcManager;
	}

	public void process() {
		XStream stream = XStreamFactory.getXStream(XSTREAM_NAME);
		for (FileObject fileObject : deleteList) {
			LOGGER.logMessage(LogLevel.INFO, "删除脚本mvc文件[{0}]开始...",
                    fileObject.getAbsolutePath());
			ScriptControllerConfigs configs = (ScriptControllerConfigs) caches.get(fileObject
                    .getAbsolutePath());
			if(configs!=null){
			   tinyScriptMvcManager.removeScriptControllerConfigs(configs);
			   caches.remove(fileObject.getAbsolutePath());
			}
			LOGGER.logMessage(LogLevel.INFO, "删除脚本mvc文件[{0}]结束!",
                    fileObject.getAbsolutePath());
		}
		
		for (FileObject fileObject : changeList) {
			LOGGER.logMessage(LogLevel.INFO, "加载脚本mvc文件[{0}]开始...",
                    fileObject.getAbsolutePath());
			ScriptControllerConfigs oldConfigs = (ScriptControllerConfigs) caches.get(fileObject
                    .getAbsolutePath());
			if(oldConfigs!=null){
				tinyScriptMvcManager.removeScriptControllerConfigs(oldConfigs);
			}
			try{
				ScriptControllerConfigs configs = convertFromXml(stream, fileObject);
				tinyScriptMvcManager.addScriptControllerConfigs(configs);
				caches.put(fileObject.getAbsolutePath(), configs);
			}catch(Exception e){
				LOGGER.errorMessage("加载脚本mvc文件[{0}]发生异常:", e, fileObject.getAbsolutePath());
			}finally{
				LOGGER.logMessage(LogLevel.INFO, "加载脚本mvc文件[{0}]结束!",
	                    fileObject.getAbsolutePath());
			}
			
		}
	}

	protected boolean checkMatch(FileObject fileObject) {
		return fileObject.getFileName().endsWith(".scriptmvc") || fileObject.getFileName().endsWith(".scriptmvc.xml");
	}

}
