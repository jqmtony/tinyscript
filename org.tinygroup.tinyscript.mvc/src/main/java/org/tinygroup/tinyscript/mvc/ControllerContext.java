package org.tinygroup.tinyscript.mvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.tinygroup.context.Context;

/**
 * 控制层上下文
 * @author yancheng11334
 *
 */
public interface ControllerContext extends Context{

	HttpServletRequest  getRequest();
	
	HttpServletResponse getResponse();
	
	ScriptController  getScriptController();
}
