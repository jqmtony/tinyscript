package org.tinygroup.tinyscript.mvc.impl;

import org.tinygroup.tinyscript.mvc.RequestMapping;

public class DefaultRequestMapping implements RequestMapping{
    
	private String name;
	private String[] path;
	private String[] method;
	private String[] params;
	private String[] headers;
	private String[] consumes;
	private String[] produces;
	
	public DefaultRequestMapping(){
		
	}
	
	public DefaultRequestMapping(String[] path){
		this(null,path,new String[]{"GET","POST"},null,null,null,null);
	}
	
	public DefaultRequestMapping(String[] path,String[] method){
		this(null,path,method,null,null,null,null);
	}
	
    public DefaultRequestMapping(String name,String[] path,String[] method,String[] params,String[] headers,String[] consumes,String[] produces){
		this.name = name;
		this.path = path;
		this.method = method;
		this.params = params;
		this.headers = headers;
		this.consumes = consumes;
		this.produces = produces;
	}
	
	public String getName() {
		return name;
	}

	public String[] getPaths() {
		return path;
	}

	public String[] getMethods() {
		return method;
	}

	public String[] getParams() {
		return params;
	}

	public String[] getHeaders() {
		return headers;
	}

	public String[] getConsumes() {
		return consumes;
	}

	public String[] getProduces() {
		return produces;
	}

}
