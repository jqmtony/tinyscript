package org.tinygroup.tinyscript.mvc;

import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author yancheng11334
 *
 */
public interface ConvertHandler<Accept> {
     
	 /**
	  * 结果对象是否能转换HTTP接受类型
	  * @param obj
	  * @param accept
	  * @return
	  */
	 boolean isMatch(Object obj,Accept accept);
	 
	 /**
	  * 执行转换
	  * @param response
	  * @param obj
	  * @param accept
	  * @throws Exception
	  */
	 void convert(HttpServletResponse response,Object obj,Accept accept) throws Exception;
}
